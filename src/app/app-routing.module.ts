import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { CourseEnrollComponent } from './admin/course-enroll/course-enroll.component';
import { CourseListComponent } from './admin/course-list/course-list.component';
import { ReportsComponent } from './admin/reports/reports.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { StudentDashboardComponent } from './student/student-dashboard/student-dashboard.component';
import { StudentListComponent } from './admin/student-list/student-list.component';
import { StudentRegistrationComponent } from './student/student-registration/student-registration.component';
import { LoginComponent } from './components/login/login.component';
import { PartnerListComponent } from './admin/partner-list/partner-list.component';
import { JobListComponent } from './admin/job-list/job-list.component';
import { JobMappingComponent } from './admin/job-mapping/job-mapping.component';

// Guards
import { StudentGuard } from './guards/student.guard';
import { AdminGuard } from './guards/admin.guard';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';

const routes: Routes = [
  // Home page
  { path: '', component: HomeComponent },
  // Student registration
  { path: 'student-register', component: StudentRegistrationComponent },
  // Login page
  { path: 'login', component: LoginComponent },
  // Admin Login page
  { path: 'admin-login', component: AdminLoginComponent },

  // Student dashboard
  {
    path: 'student-dashboard',
    component: StudentDashboardComponent,
    canActivate: [StudentGuard],
  },

  // Admin dashboard
  {
    path: 'admin-dashboard',
    component: AdminDashboardComponent,
    canActivate: [AdminGuard],
  },
  // Admin to view all students
  {
    path: 'student-list',
    component: StudentListComponent,
    canActivate: [AdminGuard],
  },
  // Admin to view all partners
  {
    path: 'partner-list',
    component: PartnerListComponent,
    canActivate: [AdminGuard],
  },
  // Admin to view all courses
  {
    path: 'course-list',
    component: CourseListComponent,
    canActivate: [AdminGuard],
  },
  // Admin to enroll students into a batch
  {
    path: 'course-enroll',
    component: CourseEnrollComponent,
    canActivate: [AdminGuard],
  },
  // Admin to view all jobs
  { path: 'job-list', component: JobListComponent, canActivate: [AdminGuard] },
  // Admin to view job mappings
  {
    path: 'job-mapping',
    component: JobMappingComponent,
    canActivate: [AdminGuard],
  },
  // Admin reports
  { path: 'reports', component: ReportsComponent, canActivate: [AdminGuard] },

  // 404 page
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
