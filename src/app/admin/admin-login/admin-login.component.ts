import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss'],
})
export class AdminLoginComponent implements OnInit {
  form: any = {
    username: '',
    password: '',
  };

  hidePass: boolean = true;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private snackbarService: MatSnackBar
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    if (this.loginService.loginAdmin(this.form)) {
      localStorage.setItem('isStudentLoggedIn', 'false');
      localStorage.setItem('isAdminLoggedIn', 'true');
      window.location.reload();
      this.snackbarService.open('Welcome to Hunar!', '', {
        duration: 5000,
      });
      this.router.navigateByUrl('/admin');
    } else {
      this.form = {
        username: '',
        password: '',
      };
      this.snackbarService.open('Invalid credentials', '', {
        duration: 5000,
      });
    }
  }

  togglePassHidden() {
    this.hidePass = !this.hidePass;
  }
}
