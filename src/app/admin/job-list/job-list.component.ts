import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Job } from 'src/app/model/job';
import { JobService } from 'src/app/service/job.service';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.scss'],
})
export class JobListComponent implements OnInit {
  displayedJobColumns: string[] = [
    'jobName',
    'jobPostedByName',
    'jobLocation',
    'jobOpenVacancy',
    'jobRole',
    'jobValidateTillEndDate',
    'salary',
    'action',
  ];

  dataSourceJob = new MatTableDataSource<Job>();

  @ViewChild('MatPaginator1')
  jobPaginator!: MatPaginator;

  @ViewChild('close') close!: ElementRef;
  @ViewChild('editClose') editClose!: ElementRef;

  viewJobInfo: Job | undefined;

  isLoading: boolean = true;

  registrationform = new FormGroup({
    jobName: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    jobDescription: new FormControl('', [
      Validators.required,
      Validators.minLength(10),
    ]),
    jobIndustry: new FormControl('', [Validators.required]),
    jobLocation: new FormControl('', [Validators.required]),
    jobOpenVacancy: new FormControl([Validators.required]),
    jobPostedByEmailId: new FormControl('', [Validators.required]),
    jobPostedByMobileNo: new FormControl([Validators.required]),
    jobPostedByName: new FormControl('', [Validators.required]),
    jobPostingDate: new FormControl('', [Validators.required]),
    jobRole: new FormControl('', [Validators.required]),
    jobType: new FormControl('', [Validators.required]),
    jobValidateTillEndDate: new FormControl('', [Validators.required]),
    maxSalary: new FormControl([Validators.required]),
    minSalary: new FormControl([Validators.required]),
    minimumQualification: new FormControl('', [Validators.required]),
    travelRequired: new FormControl('', [Validators.required]),
    additionalQualification: new FormControl('', [Validators.required]),
  });

  jobupdateform = new FormGroup({
    id: new FormControl([Validators.required]),
    jobName: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    jobDescription: new FormControl('', [
      Validators.required,
      Validators.minLength(10),
    ]),
    jobIndustry: new FormControl('', [Validators.required]),
    jobLocation: new FormControl('', [Validators.required]),
    jobOpenVacancy: new FormControl([Validators.required]),
    jobPostedByEmailId: new FormControl('', [Validators.required]),
    jobPostedByMobileNo: new FormControl([Validators.required]),
    jobPostedByName: new FormControl('', [Validators.required]),
    jobPostingDate: new FormControl('', [Validators.required]),
    jobRole: new FormControl('', [Validators.required]),
    jobType: new FormControl('', [Validators.required]),
    jobValidateTillEndDate: new FormControl('', [Validators.required]),
    maxSalary: new FormControl([Validators.required]),
    minSalary: new FormControl([Validators.required]),
    minimumQualification: new FormControl('', [Validators.required]),
    travelRequired: new FormControl('', [Validators.required]),
    additionalQualification: new FormControl('', [Validators.required]),
  });

  constructor(
    private jobService: JobService,
    private router: Router,
    private snackbarService: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.loadAllJobs();
  }

  loadAllJobs() {
    this.jobService.getAllJobs().subscribe((data: any) => {
      this.isLoading = false;
      this.dataSourceJob = new MatTableDataSource<Job>(data);
      this.dataSourceJob.paginator = this.jobPaginator;
    });
  }

  loadJobById(jobId: number) {
    this.jobService.getJobById(jobId).subscribe((data: any) => {
      this.viewJobInfo = data;
      console.log(this.viewJobInfo);
    });
  }

  update(job: any) {
    this.jobupdateform.patchValue({
      id: job.id,
      jobName: job.jobName,
      jobDescription: job.jobDescription,
      jobIndustry: job.jobIndustry,
      jobLocation: job.jobLocation,
      jobOpenVacancy: job.jobOpenVacancy,
      jobPostedByEmailId: job.jobPostedByEmailId,
      jobPostedByMobileNo: job.jobPostedByMobileNo,
      jobPostedByName: job.jobPostedByName,
      jobPostingDate: job.jobPostingDate,
      jobRole: job.jobRole,
      jobType: job.jobType,
      jobValidateTillEndDate: job.jobValidateTillEndDate,
      maxSalary: job.maxSalary,
      minSalary: job.minSalary,
      minimumQualification: job.minimumQualification,
      travelRequired: job.travelRequired,
      additionalQualification: job.additionalQualification,
    });
  }

  onUpdate() {
    if (this.jobupdateform.status == 'VALID') {
      this.jobService
        .updateJobDetails(this.jobupdateform.value)
        .subscribe((data: any) => {
          console.log(data);
          this.snackbarService.open('Updated job details successfully', '', {
            duration: 5000,
          });
          this.loadAllJobs();
        });
    } else {
      this.snackbarService.open('An error has occured while processing', '', {
        duration: 5000,
      });
    }
    this.editClose.nativeElement.click();
  }

  onSubmit() {
    if (this.registrationform.status == 'VALID') {
      this.jobService.registerNewJob(this.registrationform.value);
      this.snackbarService.open('Added job details successfully', '', {
        duration: 5000,
      });
      this.loadAllJobs();
    } else {
      this.snackbarService.open('An error has occured while processing', '', {
        duration: 5000,
      });
    }
    this.close.nativeElement.click();
  }
}
