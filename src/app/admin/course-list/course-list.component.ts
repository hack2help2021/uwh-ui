import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Course } from 'src/app/model/course';
import { CourseService } from 'src/app/service/course.service';
@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss'],
})
export class CourseListComponent implements OnInit {
  displayedCourseColumns: string[] = [
    'courseName',
    'details',
    'duration',
    'startDate',
    'endDate',
    'numberOfSeats',
    'numberOfEnrollment',
    'skills',
    'edit course',
  ];

  dataSourceCourse = new MatTableDataSource<Course>();

  @ViewChild('MatPaginator1')
  partnerPaginator!: MatPaginator;

  @ViewChild('close') close!: ElementRef;
  @ViewChild('closeEdit') closeEdit!: ElementRef;

  courseToSearch: string = '';
  filteredCourses: Course[] | undefined;

  isLoading: boolean = true;

  registrationform = new FormGroup({
    courseName: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    details: new FormControl('', [Validators.required]),
    duration: new FormControl('', [Validators.required]),
    startDate: new FormControl('', [Validators.required]),
    endDate: new FormControl('', [Validators.required]),
    numberOfSeats: new FormControl('', [Validators.required]),
    numberOfEnrollment: new FormControl('', [Validators.required]),
    skills: new FormControl('', [Validators.required]),
    partners: new FormGroup({
      id: new FormControl('', [Validators.required]),
    }),
  });

  courseUpdateform = new FormGroup({
    id: new FormControl(''),
    courseName: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    details: new FormControl('', [Validators.required]),
    duration: new FormControl('', [Validators.required]),
    startDate: new FormControl('', [Validators.required]),
    endDate: new FormControl('', [Validators.required]),
    numberOfSeats: new FormControl('', [Validators.required]),
    numberOfEnrollment: new FormControl('', [Validators.required]),
    skills: new FormControl('', [Validators.required]),
  });

  partnersList: Array<any> = [];

  constructor(
    private courseService: CourseService,
    private snackbarService: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.loadAllCourses();
    this.getPartners();
  }

  loadAllCourses() {
    this.courseService.getCourses().subscribe((data: any) => {
      this.dataSourceCourse = new MatTableDataSource<Course>(data);
      this.dataSourceCourse.paginator = this.partnerPaginator;
    });
  }

  getPartners() {
    this.courseService.getPartnersList().subscribe((response: any) => {
      this.isLoading = false;
      this.partnersList = response;
    });
  }

  update(course: any) {
    this.courseUpdateform.patchValue({
      id: course.id,
      courseName: course.courseName,
      details: course.details,
      duration: course.duration,
      startDate: course.startDate,
      endDate: course.endDate,
      numberOfSeats: course.numberOfSeats,
      numberOfEnrollment: course.numberOfEnrollment,
      skills: course.skills,
    });
  }

  onUpdate() {
    if (this.courseUpdateform.status == 'VALID') {
      this.courseService
        .updateCourse(this.courseUpdateform.value)
        .subscribe((data: any) => {
          this.snackbarService.open('Updated course details successfully', '', {
            duration: 5000,
          });
          this.loadAllCourses();
        });
    } else {
      this.snackbarService.open('An error has occured while processing', '', {
        duration: 5000,
      });
    }
    this.closeEdit.nativeElement.click();
  }

  onSubmit() {
    if (this.registrationform.status == 'VALID') {
      this.courseService.saveCourse(this.registrationform.value);
      this.snackbarService.open('Added course details successfully', '', {
        duration: 5000,
      });
      this.loadAllCourses();
    } else {
      this.snackbarService.open('An error has occured while processing', '', {
        duration: 5000,
      });
    }
    this.closeEdit.nativeElement.click();
  }

  performFilterOnCourses() {
    if (this.courseToSearch) {
      this.filteredCourses = this.dataSourceCourse.data.filter(
        (course: Course) => {
          return course.courseName
            .toLocaleLowerCase()
            .includes(this.courseToSearch.toLocaleLowerCase());
        }
      );

      this.dataSourceCourse = new MatTableDataSource<Course>(
        this.filteredCourses
      );
    } else {
      this.loadAllCourses();
    }
  }
}
