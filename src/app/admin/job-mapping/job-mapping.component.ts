import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Job } from 'src/app/model/job';
import { Student } from 'src/app/model/student';
import { JobService } from 'src/app/service/job.service';
import { StudentService } from 'src/app/service/student.service';

@Component({
  selector: 'app-job-mapping',
  templateUrl: './job-mapping.component.html',
  styleUrls: ['./job-mapping.component.scss'],
})
export class JobMappingComponent implements OnInit {
  jobList: Job[] | undefined;
  studentList: Student[] | undefined;

  isLoading: boolean = false;

  jobToView: Job | undefined;

  jobregistrationform = new FormGroup({
    jobs: new FormGroup({
      id: new FormControl([Validators.required]),
    }),
    students: new FormControl([Validators.required]),
  });

  jobviewform = new FormGroup({
    id: new FormControl([Validators.required]),
  });

  constructor(
    private jobService: JobService,
    private studentService: StudentService,
    private snackbarService: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.jobService.getAllJobs().subscribe((data: any) => {
      this.jobList = data;
    });
    this.studentService.getAllStudents().subscribe((data: any) => {
      this.studentList = data;
    });
  }

  onSubmit() {
    const jobId = this.jobregistrationform.value.jobs.id;
    const studentIds = this.jobregistrationform.value.students;
    let url = `job/enrollStudent/${jobId}?studentIds=`;

    for (let index = 1; index <= studentIds.length; index++) {
      if (studentIds.length == 1 || index == 1) {
        url += `${studentIds[0]}`;
      } else {
        url += `&studentIds=${studentIds[index - 1]}`;
      }
    }

    this.jobService.mapStudentToJob(url).subscribe((data: any) => {
      console.log(data);
      this.snackbarService.open('Job mapping successfully', '', {
        duration: 5000,
      });
    });
  }

  onView() {
    this.isLoading = true;
    if (this.jobviewform.status == 'VALID') {
      this.jobService
        .getJobById(this.jobviewform.value)
        .subscribe((data: any) => {
          this.jobToView = data;
        });
    } else {
      console.log('Invalid input');
    }
  }
}
