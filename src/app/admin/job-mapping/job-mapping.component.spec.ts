import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobMappingComponent } from './job-mapping.component';

describe('JobMappingComponent', () => {
  let component: JobMappingComponent;
  let fixture: ComponentFixture<JobMappingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobMappingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
