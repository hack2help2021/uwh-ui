import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Batch } from 'src/app/model/batch';
import { Course } from 'src/app/model/course';
import { BatchService } from 'src/app/service/batch.service';
import { CourseService } from 'src/app/service/course.service';

@Component({
  selector: 'app-course-enroll',
  templateUrl: './course-enroll.component.html',
  styleUrls: ['./course-enroll.component.scss'],
})
export class CourseEnrollComponent implements OnInit {
  displayedBatchesColumns: string[] = [
    'courses',
    'location',
    'startDate',
    'endDate',
    'students',
  ];

  courseList: Course[] | undefined;

  dataSourceBatches = new MatTableDataSource<Batch>();

  @ViewChild('MatPaginator1')
  batchPaginator!: MatPaginator;

  isLoading: boolean = false;

  batchregistrationform = new FormGroup({
    courses: new FormGroup({
      id: new FormControl('', [Validators.required]),
    }),
    startDate: new FormControl('', [Validators.required]),
    endDate: new FormControl('', [Validators.required]),
    location: new FormControl('', [Validators.required]),
  });

  batchviewform = new FormGroup({
    courses: new FormGroup({
      id: new FormControl('', [Validators.required]),
    }),
  });

  constructor(
    private batchService: BatchService,
    private courseService: CourseService,
    private snackbarService: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.getAllCourses();
  }

  getAllCourses() {
    this.courseService.getCourses().subscribe((data: any) => {
      this.courseList = data;
    });
  }

  onSubmit() {
    if (this.batchregistrationform.status == 'VALID') {
      console.log(this.batchregistrationform.value);
      this.batchService
        .createNewBatch(this.batchregistrationform.value)
        .subscribe((data: any) => {
          this.snackbarService.open('Added batch details successfully', '', {
            duration: 5000,
          });
        });
    } else {
      this.snackbarService.open('An error has occured while processing', '', {
        duration: 5000,
      });
    }
  }

  onView() {
    this.isLoading = true;
    if (this.batchviewform.status == 'VALID') {
      console.log(this.batchviewform.value);
      this.batchService
        .getAllBatchesByCourseId(this.batchviewform.value)
        .subscribe((data: any) => {
          this.isLoading = false;
          this.dataSourceBatches = new MatTableDataSource<Batch>(data);
          this.dataSourceBatches.paginator = this.batchPaginator;
        });
    } else {
      this.snackbarService.open('An error has occured while processing', '', {
        duration: 5000,
      });
    }
  }
}
