import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { ReportsComponent } from './reports/reports.component';
import { CourseListComponent } from './course-list/course-list.component';
import { CourseEnrollComponent } from './course-enroll/course-enroll.component';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { StudentListComponent } from './student-list/student-list.component';
import { PartnerListComponent } from './partner-list/partner-list.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { HttpClientModule } from '@angular/common/http';
import { JobListComponent } from './job-list/job-list.component';
import { MatSelectModule } from '@angular/material/select';
import { JobMappingComponent } from './job-mapping/job-mapping.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AdminLoginComponent } from './admin-login/admin-login.component';

@NgModule({
  declarations: [
    AdminDashboardComponent,
    ReportsComponent,
    CourseListComponent,
    CourseEnrollComponent,
    StudentListComponent,
    PartnerListComponent,
    JobListComponent,
    JobMappingComponent,
    AdminLoginComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatPaginatorModule,
    MatTableModule,
    HttpClientModule,
    MatSelectModule,
    MatDatepickerModule,
    MatRadioModule,
    MatProgressSpinnerModule,
  ],
})
export class AdminModule {}
