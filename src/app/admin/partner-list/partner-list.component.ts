import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Partner } from '../../model/partner';
import { PartnerService } from '../../service/partner.service';

@Component({
  selector: 'app-partner-list',
  templateUrl: './partner-list.component.html',
  styleUrls: ['./partner-list.component.scss'],
})
export class PartnerListComponent implements OnInit {
  displayedPartnerColumns: string[] = [
    'partnerName',
    'address',
    'phone1',
    'email',
    'edit partner',
  ];

  dataSourcePartner = new MatTableDataSource<Partner>();

  @ViewChild('MatPaginator1')
  partnerPaginator!: MatPaginator;

  @ViewChild('close') close!: ElementRef;
  @ViewChild('closeEdit') closeEdit!: ElementRef;

  partnerToSearch: string = '';
  filteredPartners: Partner[] | undefined;

  isLoading: boolean = true;

  registrationform = new FormGroup({
    partnerName: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    address: new FormControl('', [Validators.required]),
    phone1: new FormControl('', [Validators.required]),
    phone2: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
  });

  partnerupdateform = new FormGroup({
    id: new FormControl('', [Validators.required]),
    partnerName: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    address: new FormControl('', [Validators.required]),
    phone1: new FormControl('', [Validators.required]),
    phone2: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
  });

  constructor(
    private partnerService: PartnerService,
    private snackbarService: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.loadAllPartners();
  }

  loadAllPartners() {
    return this.partnerService.getAllPartners().subscribe((data: any) => {
      this.isLoading = false;
      this.dataSourcePartner = new MatTableDataSource<Partner>(data);
      this.dataSourcePartner.paginator = this.partnerPaginator;
    });
  }

  update(partner: any) {
    this.partnerupdateform.patchValue({
      id: partner.id,
      partnerName: partner.partnerName,
      address: partner.address,
      phone1: partner.phone1,
      phone2: partner.phone2,
      email: partner.email,
    });
    console.log(this.partnerupdateform.value);
  }

  onUpdate() {
    if (this.partnerupdateform.status == 'VALID') {
      this.partnerService
        .updatePartnerDetails(this.partnerupdateform.value)
        .subscribe((data: any) => {
          this.snackbarService.open(
            'Updated partner details successfully',
            '',
            {
              duration: 5000,
            }
          );
          this.loadAllPartners();
        });
    } else {
      this.snackbarService.open('An error has occured while processing', '', {
        duration: 5000,
      });
    }
    this.closeEdit.nativeElement.click();
  }

  onSubmit() {
    if (this.registrationform.status == 'VALID') {
      this.partnerService.registerNewPartner(this.registrationform.value);
      this.snackbarService.open('Added partner details successfully', '', {
        duration: 5000,
      });
      this.loadAllPartners();
    } else {
      this.snackbarService.open('An error has occured while processing', '', {
        duration: 5000,
      });
    }
    this.close.nativeElement.click();
  }

  performFilterOnPartners() {
    if (this.partnerToSearch) {
      this.filteredPartners = this.dataSourcePartner.data.filter(
        (partner: Partner) => {
          return partner.partnerName
            .toLocaleLowerCase()
            .includes(this.partnerToSearch.toLocaleLowerCase());
        }
      );

      this.dataSourcePartner = new MatTableDataSource<Partner>(
        this.filteredPartners
      );
    } else {
      this.loadAllPartners();
    }
  }
}
