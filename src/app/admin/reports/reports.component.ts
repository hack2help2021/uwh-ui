import { Component, OnInit, ViewChild } from '@angular/core';
import { ReportsService } from 'src/app/service/reports.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ExcelService  } from 'src/app/service/excel.service';
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  excel: Array<any> = [];

  constructor(private reportsService: ReportsService,
    private excelService: ExcelService) { }
  form: any = {
    centerName: null,
    gender: null,
    highestEducation: null,
    enrolledInCourse: null,
    experienceInYears: null,
    areaOfInterest: null,
    industry: null,
    registrationDate: {
      from: null,
      to: null
    }
  };
  displayedCourseColumns: string[] = [
    'uid',
    'regdNo',
    'studentName',
    'gender',
    'dateOfBirth',
    'qualification',
    'studentCurrentSkills',
    'experience',
    'maxSalary',
    'jobName',
    'jobDescription',
    'jobType',
    'jobStatus',
    'jobLocation'
  ];

  dataSourceCourse = new MatTableDataSource<any>();
  @ViewChild('MatPaginator1')
  partnerPaginator!: MatPaginator;
  ngOnInit(): void {
  }

  onSubmit() {
    console.log('form', this.form);
  }

  onView() {
    this.reportsService.getViewData(this.form).subscribe((response: any) => {
      this.dataSourceCourse = new MatTableDataSource<any>(response.content);
      response.content.forEach((element: any) => {
        const item = {...element, ...element.student};
        delete item.student;
        this.excel.push(item);
      });
      this.dataSourceCourse.paginator = this.partnerPaginator;
    });
  }

  excelDownload() {
    // this.reportsService.downloadExcel(this.form).subscribe(response => { });
    this.reportsService.getViewData(this.form).subscribe((response: any) => {
      this.excel = [];
      response.content.forEach((element: any) => {
        const item = {...element, ...element.student};
        delete item.student;
        this.excel.push(item);
      });
      this.excelService.exportAsExcelFile(this.excel, 'studentData');
    });
  }

  pdfDownload() {
    this.reportsService.downloadPdf(this.form).subscribe(response => {

    });
  }
}
