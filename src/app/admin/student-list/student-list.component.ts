import {
  Component,
  ElementRef,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Student } from '../../model/student';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { StudentService } from '../../service/student.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss'],
})
export class StudentListComponent implements OnInit {
  displayedStudentColumns: string[] = [
    'uid',
    'regdNo',
    'studentName',
    'qualification',
    'university',
    'studentCurrentSkills',
    'studentCurrentJobFields',
    'experience',
    'dateOfBirth',
    'action',
  ];

  dataSourceStudent = new MatTableDataSource<Student>();

  calculateAge(date: any) {
    let ageDate = new Date(date);
    let timeDiff = Math.abs(Date.now() - ageDate.getTime());
    let age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
    return age;
  }

  @ViewChild('MatPaginator1')
  studentPaginator!: MatPaginator;

  @ViewChild('close') close!: ElementRef;
  @ViewChild('editClose') editClose!: ElementRef;

  studentToSearch: string = '';
  filteredStudents: Student[] | undefined;

  viewStudentInfo: Student | undefined;

  isLoading: boolean = true;

  registrationform = new FormGroup({
    uid: new FormControl('', [Validators.required]),
    uidType: new FormControl('', [Validators.required]),
    regdNo: new FormControl('', [Validators.required]),
    role: new FormControl('student'),
    studentName: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    qualification: new FormControl('', [Validators.required]),
    university: new FormControl('', [Validators.required]),
    studentCurrentSkills: new FormControl('', [Validators.required]),
    studentCurrentJobFields: new FormControl('', [Validators.required]),
    experience: new FormControl('', [Validators.required]),
    dateOfBirth: new FormControl('', [Validators.required]),
  });

  studentupdateform = new FormGroup({
    id: new FormControl(''),
    uid: new FormControl('', [Validators.required]),
    regdNo: new FormControl('', [Validators.required]),
    studentName: new FormControl('', [Validators.required]),
    qualification: new FormControl('', [Validators.required]),
    university: new FormControl('', [Validators.required]),
    studentCurrentSkills: new FormControl('', [Validators.required]),
    studentCurrentJobFields: new FormControl('', [Validators.required]),
    experience: new FormControl('', [Validators.required]),
    dateOfBirth: new FormControl('', [Validators.required]),
  });

  constructor(
    private studentService: StudentService,
    private snackbarService: MatSnackBar,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.loadAllStudents();
  }

  loadAllStudents() {
    this.studentService.getAllStudents().subscribe((data: any) => {
      this.isLoading = false;
      this.dataSourceStudent = new MatTableDataSource<Student>(data);
      this.dataSourceStudent.paginator = this.studentPaginator;
    });
  }

  onUpdate() {
    if (this.studentupdateform.status == 'VALID') {
      console.log(this.studentupdateform.value);
      this.studentService
        .updateStudent(this.studentupdateform.value)
        .subscribe((data: any) => {
          console.log(data);
          this.snackbarService.open(
            'Updated student details successfully',
            '',
            {
              duration: 5000,
            }
          );
          this.loadAllStudents();
        });
    } else {
      this.snackbarService.open('An error has occured while processing', '', {
        duration: 5000,
      });
    }
    this.studentupdateform.reset();
    this.editClose.nativeElement.click();
  }

  onSubmit() {
    if (this.registrationform.status == 'VALID') {
      console.log(this.registrationform.value);
      this.studentService
        .registerStudent(this.registrationform.value)
        .subscribe((data: any) => {
          this.snackbarService.open('Added student details successfully', '', {
            duration: 5000,
          });
          this.loadAllStudents();
        });
    } else {
      this.snackbarService.open('An error has occured while processing', '', {
        duration: 5000,
      });
    }
    this.registrationform.reset();
    this.close.nativeElement.click();
  }

  update(student: any) {
    this.studentupdateform.patchValue({
      id: student.id,
      uid: student.uid,
      regdNo: student.regdNo,
      studentName: student.studentName,
      qualification: student.qualification,
      university: student.university,
      studentCurrentSkills: student.studentCurrentSkills,
      studentCurrentJobFields: student.studentCurrentJobFields,
      experience: student.experience,
      dateOfBirth: student.dateOfBirth,
    });
  }

  view(studentId: any) {
    this.studentService.getStudentById(studentId).subscribe((data) => {
      this.viewStudentInfo = data;

      this.dialog.open(ViewDialog, {
        width: '550px',
        maxHeight: '80vh',
        data: { student: this.viewStudentInfo },
      });
    });
  }

  performFilterOnStudents() {
    if (this.studentToSearch) {
      this.filteredStudents = this.dataSourceStudent.data.filter(
        (student: Student) => {
          return student.studentName
            .toLocaleLowerCase()
            .includes(this.studentToSearch.toLocaleLowerCase());
        }
      );

      this.dataSourceStudent = new MatTableDataSource<Student>(
        this.filteredStudents
      );
    } else {
      this.loadAllStudents();
    }
  }
}

@Component({
  selector: 'view-dialog',
  template: `
    <div class="row">
      <div class="col-md-10">
        <h1 mat-dialog-title>{{ s.student.studentName }}</h1>
      </div>
      <div class="col-md-2">
        <button class="close" mat-icon-button (click)="onClose()">X</button>
      </div>
    </div>
    <div mat-dialog-content class="mat-typography">
      <!-- Basic details -->
      <section class="mb-2">
        <h3 class="font-weight-bold">Basic details</h3>
        <div class="row">
          <div class="col-md-6">
            <label>UID</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.uid }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Registration No</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.regNo }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Registration Date</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.regDate }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Purpose</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.purpose }}</p>
          </div>
        </div>
      </section>

      <!-- Education background -->
      <section class="mb-2">
        <h3 class="font-weight-bold">Education background</h3>
        <div class="row">
          <div class="col-md-6">
            <label>Qualification</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.qualification }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Field of work</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.studentCurrentJobFields }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>University</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.university }}</p>
          </div>
        </div>
      </section>

      <!-- Skills -->
      <section class="mb-2">
        <h3 class="font-weight-bold">Skills</h3>
        <div class="row">
          <div class="col-md-6">
            <label>Current skills</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.studentCurrentSkills }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Interested skills</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.studentInterestedSkills }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Additional course</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.additionalCourse }}</p>
          </div>
        </div>
      </section>

      <!-- Job details -->
      <section class="mb-2">
        <h3 class="font-weight-bold">Job details</h3>
        <div class="row">
          <div class="col-md-6">
            <label>Current skills</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.studentCurrentSkills }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Current salary</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.currentSalary }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Expected salary</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.expectedJobSalary }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Experience</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.experience }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Interested fields</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.studentInterestedJobFields }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Location preference</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.studentJobLocationPreferences }}</p>
          </div>
        </div>
      </section>

      <!-- Personal details -->
      <section class="mb-2">
        <h3 class="font-weight-bold">Personal details</h3>
        <div class="row">
          <div class="col-md-6">
            <label>Email ID</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.email }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Phone No</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.phone }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Alternative Phone</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.additionalPhone }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Date of Birth</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.dateOfBirth }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Gender</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.gender }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>BPL Card No</label>
          </div>
          <div class="col-md-6">
            <p>{{ s.student.bplCard }}</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label>Address</label>
          </div>
          <div class="col-md-6">
            <p>
              {{ s.student.currentAddress }}
              {{ s.student.currentCity }} {{ s.student.districtName }}
              {{ s.student.pinCode }}
            </p>
          </div>
        </div>
      </section>
    </div>
  `,
})
export class ViewDialog {
  constructor(
    public dialogRef: MatDialogRef<ViewDialog>,
    @Inject(MAT_DIALOG_DATA) public s: any
  ) {}

  onClose(): void {
    this.dialogRef.close();
  }
}
