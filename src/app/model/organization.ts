export interface Organization {
  id: number;
  orgName: string;
  address: string;
  email: string;
  phone1: string;
  phone2: string;
}
