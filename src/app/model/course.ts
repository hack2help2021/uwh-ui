import { Partner } from './partner';

export interface Course {
  id: number;
  courseName: string;
  skills: string;
  duration: number;
  numberOfSeats: number;
  numberOfEnrollment: number;
  numberOf?: number;
  details: string;
  fileLocation?: string;
  startDate: string;
  endDate: string;
  partners: Partner;
}
