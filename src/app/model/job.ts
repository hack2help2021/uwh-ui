import { Course } from './course';
import { Student } from './student';

export interface Job {
  id: number;
  jobName: string;
  jobDescription: string;
  jobIndustry: string;
  jobLocation: string;
  jobOpenVacancy: number;
  jobPostedByEmailId: string;
  jobPostedByMobileNo: number;
  jobPostedByName: string;
  jobPostingDate: string;
  jobRole: string;
  jobType: string;
  jobValidateTillEndDate: string;
  maxSalary: number;
  minSalary: number;
  minimumQualification: string;
  students: Student[];
  travelRequired: true;
  additionalQualification: string;
  courses: Course;
}
