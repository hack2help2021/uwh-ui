import { Course } from './course';
import { Student } from './student';

export interface Batch {
  id: number;
  startDate: string;
  endDate: string;
  location: string;
  courses: Course;
  students: Student[];
}
