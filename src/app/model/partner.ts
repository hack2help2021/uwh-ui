import { Organization } from './organization';

export interface Partner {
  id: number;
  partnerName: string;
  address: string;
  phone1: string;
  phone2: string;
  email: string;
  organization: Organization;
}
