import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { Batch } from '../model/batch';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class BatchService {
  constructor(private httpService: HttpClient) {}

  getAllBatchesByCourseId(batchObj: Batch): Observable<Batch> {
    return this.httpService.get<Batch>(
      `${environment.backendApi}/batches/all/${batchObj.courses.id}`
    );
  }

  createNewBatch(batchObj: Batch): Observable<any> {
    return this.httpService.post(
      `${environment.backendApi}/batches/save?courses.id=${batchObj.courses.id}&endDate=${batchObj.endDate}&location=${batchObj.location}&startDate=${batchObj.startDate}`,
      httpOptions
    );
  }
}
