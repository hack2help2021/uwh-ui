import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private httpService: HttpClient) {}

  getUserToken() {
    const studentToken = localStorage.getItem('isStudentLoggedIn');
    const adminToken = localStorage.getItem('isAdminLoggedIn');

    if (studentToken == 'true') {
      return 'student';
    } else if (adminToken == 'true') {
      return 'admin';
    } else {
      return '';
    }
  }

  loginAdmin(formObj: any): any {
    if (formObj.username == 'admin' && formObj.password == 'WeKare@123') {
      return true;
    } else {
      return false;
    }
  }

  logInUser(formObj: any): any {
    return this.httpService.post(
      `${environment.backendApi}/login`,
      formObj,
      httpOptions
    );
  }
}
