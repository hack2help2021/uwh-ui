import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { Job } from '../model/job';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class JobService {
  constructor(private httpService: HttpClient) {}

  getAllJobs(): Observable<Job> {
    return this.httpService.get<Job>(`${environment.backendApi}/job/all/`);
  }

  getJobById(jobObj: any): Observable<any> {
    return this.httpService.get<any>(
      `${environment.backendApi}/job/all/${jobObj.id}`
    );
  }

  updateJobDetails(jobDetails: any): any {
    return this.httpService.put(
      `${environment.backendApi}/job/update?id=${jobDetails.id}&additionalQualification=${jobDetails.additionalQualification}
        &jobDescription=${jobDetails.jobDescription}&jobIndustry=${jobDetails.jobIndustry}&jobLocation=${jobDetails.jobLocation}&jobName=${jobDetails.jobName}
        &jobOpenVacancy=${jobDetails.jobOpenVacancy}&jobPostedByEmailId=${jobDetails.jobPostedByEmailId}&jobPostedByMobileNo=${jobDetails.jobPostedByMobileNo}
        &jobPostedByName=${jobDetails.jobPostedByName}&jobPostingDate=${jobDetails.jobPostingDate}&jobRole=${jobDetails.jobRole}&jobType=${jobDetails.jobType}
        &jobValidateTillEndDate=${jobDetails.jobValidateTillEndDate}&maxSalary=${jobDetails.maxSalary}&minimumQualification=${jobDetails.minimumQualification}
        &minSalary=${jobDetails.minSalary}&travelRequired=${jobDetails.travelRequired}`,
      httpOptions
    );
  }

  registerNewJob(jobDetails: any): any {
    this.httpService
      .post<any>(
        `${environment.backendApi}/job/save?additionalQualification=${jobDetails.additionalQualification}
        &jobDescription=${jobDetails.jobDescription}&jobIndustry=${jobDetails.jobIndustry}&jobLocation=${jobDetails.jobLocation}&jobName=${jobDetails.jobName}
        &jobOpenVacancy=${jobDetails.jobOpenVacancy}&jobPostedByEmailId=${jobDetails.jobPostedByEmailId}&jobPostedByMobileNo=${jobDetails.jobPostedByMobileNo}
        &jobPostedByName=${jobDetails.jobPostedByName}&jobPostingDate=${jobDetails.jobPostingDate}&jobRole=${jobDetails.jobRole}&jobType=${jobDetails.jobType}
        &jobValidateTillEndDate=${jobDetails.jobValidateTillEndDate}&maxSalary=${jobDetails.maxSalary}&minimumQualification=${jobDetails.minimumQualification}
        &minSalary=${jobDetails.minSalary}&travelRequired=${jobDetails.travelRequired}`,
        httpOptions
      )
      .subscribe((data: Object | undefined) => {
        console.log(data);
      });
  }

  mapStudentToJob(modifiedUrl: string): any {
    return this.httpService.post(
      `${environment.backendApi}/${modifiedUrl}`,
      httpOptions
    );
  }
}
