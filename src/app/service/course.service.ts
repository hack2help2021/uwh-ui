import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class CourseService {
  constructor(private http: HttpClient) {}

  getCourses() {
    return this.http.get(`${environment.backendApi}/course/all`);
  }

  getPartnersList() {
    return this.http.get(`${environment.backendApi}/partner/all`);
  }

  saveCourse(reqObj: any) {
    return this.http.post(
      `${environment.backendApi}/course/save?courseName=${reqObj.courseName}&details=${reqObj.details}&duration=${reqObj.duration}&numberOfSeats=${reqObj.numberOfSeats}&partners.id=${reqObj.partnerid}`,
      reqObj,
      httpOptions
    );
  }

  updateCourse(reqObj: any): any {
    return this.http.put(
      `${environment.backendApi}/course/update?courseName=${reqObj.courseName}&details=${reqObj.details}&duration=${reqObj.duration}&endDate=${reqObj.endDate}&id=${reqObj.id}&numberOfEnrollment=${reqObj.numberOfEnrollment}&numberOfSeats=${reqObj.numberOfSeats}&skills=${reqObj.skills}&startDate=${reqObj.startDate}`,
      httpOptions
    );
  }
}
