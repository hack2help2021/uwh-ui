import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Partner } from '../model/partner';

import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class PartnerService {
  constructor(private httpService: HttpClient) {}

  getAllPartners(): Observable<Partner> {
    return this.httpService.get<Partner>(
      `${environment.backendApi}/partner/all`
    );
  }

  registerNewPartner(formData: any): any {
    this.httpService
      .post<any>(
        `${environment.backendApi}/partner/save?address=${formData.address}&email=${formData.email}&organization.id=1&partnerName=${formData.partnerName}&phone1=${formData.phone1}&phone2=${formData.phone2}`,
        httpOptions
      )
      .subscribe((data: Object | undefined) => {
        console.log(data);
      });
  }

  updatePartnerDetails(formData: any): any {
    return this.httpService.put(
      `${environment.backendApi}/partner/update?address=${formData.address}&email=${formData.email}&id=${formData.id}&organization.id=1&partnerName=${formData.partnerName}&phone1=${formData.phone1}&phone2=${formData.phone2}`,
      httpOptions
    );
  }
}
