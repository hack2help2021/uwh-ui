import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class ReportsService {
  constructor(private http: HttpClient) {}

  getViewData(reqObj: any) {
    return this.http.post(`${environment.backendApi}/reports/view`, reqObj);
  }

  downloadExcel(reqObj: any) {
    return this.http.post(`${environment.backendApi}/reports/generateExcel`, reqObj);
  }

  downloadPdf(reqObj: any) {
    return this.http.post(`${environment.backendApi}/reports/generatePDF`, reqObj);
  }
}
