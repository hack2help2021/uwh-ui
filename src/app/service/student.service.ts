import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Student } from '../model/student';

import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class StudentService {
  constructor(private httpService: HttpClient) {}

  getAllStudents(): Observable<Student> {
    return this.httpService.get<Student>(`${environment.backendApi}/students`);
  }

  registerStudent(requestObj: any): Observable<any> {
    return this.httpService.post(
      `${environment.backendApi}/student`,
      requestObj,
      httpOptions
    );
  }

  updateStudent(student: any): any {
    return this.httpService.patch(
      `${environment.backendApi}/student/${student.id}`,
      student,
      httpOptions
    );
  }

  getStudentById(studentId: any): Observable<Student> {
    return this.httpService.get<Student>(
      `${environment.backendApi}/students/${studentId}`
    );
  }
}
