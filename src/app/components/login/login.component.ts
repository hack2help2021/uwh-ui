import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { LoginService } from '../../service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  form: any = {
    uid: '',
    uidType: '',
    password: '',
    role: 'student',
  };

  hidePass: boolean = true;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private snackbarService: MatSnackBar
  ) {}

  ngOnInit(): void {}

  onSubmit(): void {
    this.loginService.logInUser(this.form).subscribe((data: any) => {
      if (data != null) {
        window.location.reload();
        localStorage.setItem('studentId', data.id.toString());
        localStorage.setItem('isStudentLoggedIn', 'true');
        localStorage.setItem('isAdminLoggedIn', 'false');
        this.snackbarService.open('Welcome to Hunar!', '', {
          duration: 5000,
        });
      } else {
        this.form = {
          uid: '',
          uidType: '',
          password: '',
          role: 'student',
        };
        this.snackbarService.open('Invalid credentials', '', {
          duration: 5000,
        });
      }
    });
  }

  togglePassHidden() {
    this.hidePass = !this.hidePass;
  }

  navigateToRegister() {
    this.router.navigate(['/student-register']);
  }
}
