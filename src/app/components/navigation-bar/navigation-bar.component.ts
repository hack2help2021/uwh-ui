import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss'],
})
export class NavigationBarComponent implements OnInit {
  isStudentLoggedIn: boolean = false;
  isAdminLoggedIn: boolean = false;

  constructor(private router: Router, private snackbarService: MatSnackBar) {}

  ngOnInit(): void {
    if (localStorage.getItem('isStudentLoggedIn') === 'true') {
      this.isStudentLoggedIn = true;
      this.router.navigateByUrl('/student-dashboard');
    } else if (localStorage.getItem('isAdminLoggedIn') === 'true') {
      this.isAdminLoggedIn = true;
      this.router.navigateByUrl('/admin-dashboard');
    }
  }

  logOutUser() {
    this.router.navigateByUrl('/').then(() => {
      localStorage.clear();
      window.location.reload();
      this.snackbarService.open('Logged out successfully!', '', {
        duration: 5000,
      });
    });
  }
}
