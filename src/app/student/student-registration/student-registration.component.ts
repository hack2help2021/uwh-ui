import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StudentService } from 'src/app/service/student.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-student-registration',
  templateUrl: './student-registration.component.html',
  styleUrls: ['./student-registration.component.scss'],
})
export class StudentRegistrationComponent implements OnInit {
  form: any = {
    dateOfBirth: '',
    email: '',
    gender: '',
    phone: '',
    studentName: '',
    uid: '',
    uidType: '',
    password: '',
    role: 'student',
  };

  constructor(
    private studentService: StudentService,
    private router: Router,
    private snackbarService: MatSnackBar
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    if (
      this.form.studentName &&
      this.form.dateOfBirth &&
      this.form.email &&
      this.form.gender &&
      this.form.phone &&
      this.form.uid &&
      this.form.uidType &&
      this.form.password
    ) {
      this.studentService.registerStudent(this.form).subscribe(
        (data) => {
          console.log(data);
          this.form = {
            dateOfBirth: '',
            email: '',
            gender: '',
            phone: '',
            studentName: '',
            uid: '',
            uidType: '',
            password: '',
            role: 'student',
          };
          this.router.navigateByUrl('/login');
          this.snackbarService.open('Registration is successful!', '', {
            duration: 5000,
          });
        },
        (err) => {
          console.log(err);
          this.snackbarService.open(
            'An error occured in processing, please try again!',
            '',
            {
              duration: 5000,
            }
          );
        }
      );
    } else {
      this.snackbarService.open('Enter all details to proceed', '', {
        duration: 5000,
      });
    }
  }
}
