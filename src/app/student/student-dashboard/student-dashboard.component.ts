import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Student } from 'src/app/model/student';
import { StudentService } from 'src/app/service/student.service';

@Component({
  selector: 'app-student-dashboard',
  templateUrl: './student-dashboard.component.html',
  styleUrls: ['./student-dashboard.component.scss'],
})
export class StudentDashboardComponent implements OnInit {
  constructor(
    private studentService: StudentService,
    private snackbarService: MatSnackBar
  ) {}

  student: Student | undefined;

  @ViewChild('editClose') editClose!: ElementRef;

  studentupdateform = new FormGroup({
    id: new FormControl(''),
    uid: new FormControl('', [Validators.required]),
    studentName: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    gender: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.required]),
    additionalPhone: new FormControl(''),
    bplCard: new FormControl('', [Validators.required]),
    purpose: new FormControl(''),
    dateOfBirth: new FormControl('', [Validators.required]),
    qualification: new FormControl('', [Validators.required]),
    university: new FormControl('', [Validators.required]),
    experience: new FormControl('', [Validators.required]),
    additionalCourse: new FormControl(''),
    currentSalary: new FormControl(''),
    expectedJobSalary: new FormControl('', [Validators.required]),
    studentCurrentSkills: new FormControl('', [Validators.required]),
    studentCurrentJobFields: new FormControl('', [Validators.required]),
    studentInterestedSkills: new FormControl('', [Validators.required]),
    studentInterestedJobFields: new FormControl('', [Validators.required]),
    studentJobLocationPreferences: new FormControl(''),
    currentAddress: new FormControl('', [Validators.required]),
    currentCity: new FormControl('', [Validators.required]),
    districtName: new FormControl('', [Validators.required]),
    referredByType: new FormControl('', [Validators.required]),
    referredByName: new FormControl('', [Validators.required]),
  });

  ngOnInit(): void {
    const studentId = localStorage.getItem('studentId');
    this.studentService.getStudentById(studentId).subscribe((data: Student) => {
      this.student = data;
    });
  }

  onUpdate() {
    if (this.studentupdateform.status == 'VALID') {
      console.log(this.studentupdateform.value);
      this.studentService
        .updateStudent(this.studentupdateform.value)
        .subscribe((data: any) => {
          console.log(data);
          this.snackbarService.open(
            'Updated student details successfully',
            '',
            {
              duration: 5000,
            }
          );
          this.ngOnInit();
        });
    } else {
      this.snackbarService.open('An error has occured while processing', '', {
        duration: 5000,
      });
    }
    this.studentupdateform.reset();
    this.editClose.nativeElement.click();
  }

  update(student: any) {
    this.studentupdateform.patchValue({
      id: student.id,
      uid: student.uid,
      studentName: student.studentName,
      email: student.email,
      gender: student.gender,
      phone: student.phone,
      additionalPhone: student.additionalPhone,
      bplCard: student.bplCard,
      purpose: student.purpose,
      dateOfBirth: student.dateOfBirth,
      qualification: student.qualification,
      university: student.university,
      experience: student.experience,
      additionalCourse: student.additionalCourse,
      currentSalary: student.currentSalary,
      expectedJobSalary: student.expectedJobSalary,
      studentCurrentSkills: student.studentCurrentSkills,
      studentCurrentJobFields: student.studentCurrentJobFields,
      studentInterestedSkills: student.studentInterestedSkills,
      studentInterestedJobFields: student.studentInterestedJobFields,
      studentJobLocationPreferences: student.studentJobLocationPreferences,
      currentAddress: student.currentAddress,
      currentCity: student.currentCity,
      districtName: student.districtName,
      referredByType: student.referredByType,
      referredByName: student.referredByName,
    });
  }
}
