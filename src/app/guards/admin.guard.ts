import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginService } from '../service/login.service';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {
  constructor(private loginService: LoginService, private router: Router) {}

  canActivate(): any {
    if (this.loginService.getUserToken() === 'admin') {
      return true;
    } else {
      this.router.navigateByUrl('404');
      return false;
    }
  }
}
